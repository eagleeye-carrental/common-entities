package commonentity.user;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
@Data
@NoArgsConstructor
public class UserDto {
    private String id;
    private String firstName;
    private String lastName;
    private String address1;
    private String address2;
    private String city;
    private String state;
    private String postcode;
    private String country;
    private String email;
    private String phoneNumber;
    private String mobileNumber;
    private String drivingLicenseNumber;
    private LocalDate dob;

    public UserDto(UserDto userDto) {
        this.id = id;
        this.firstName = userDto.firstName;
        this.lastName = userDto.lastName;
        this.address1 = userDto.address1;
        this.address2 = userDto.address2;
        this.city = userDto.city;
        this.state = userDto.state;
        this.postcode = userDto.postcode;
        this.country = userDto.country;
        this.email = userDto.email;
        this.phoneNumber = userDto.phoneNumber;
        this.mobileNumber = userDto.mobileNumber;
        this.drivingLicenseNumber = userDto.drivingLicenseNumber;
        this.dob = userDto.dob;
    }



}
