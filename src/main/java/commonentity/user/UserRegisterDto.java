package commonentity.user;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserRegisterDto implements Serializable {
    @NotBlank(message = "First Name Required!")
    private String firstName;
    private String middleName;
    @NotBlank(message = "Last Name Required!")
    private String lastName;
    @NotBlank(message = "Email Required!")
    private String email;
    @NotBlank(message = "Mobile Number Required!")
    private String mobileNumber;
    @NotBlank(message = "Password Required!")
    private String password;
    private String re_password;

}
