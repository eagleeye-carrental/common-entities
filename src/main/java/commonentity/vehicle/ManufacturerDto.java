package commonentity.vehicle;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Data
public class ManufacturerDto implements Serializable {
    private String id;
    @NotBlank(message = "manufacture name required!")
    private String manufacturerName;
}
