package commonentity.vehicle;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Date;

@Data
public class DiscountDto implements Serializable {
    private String id;
    @NotNull(message = "Discount Percentage cannot be null")
    @Min(value = 0, message = "Discount should be greater than 0")
    private double discountPercentage;
    @NotBlank(message = "Discount code Empty")
    private String discountCode;

    @NotNull(message = "Date required!")
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date discountValidity;
}
