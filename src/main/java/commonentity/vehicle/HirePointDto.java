package commonentity.vehicle;

import lombok.Data;

import javax.validation.constraints.*;
import java.io.Serializable;

@Data
public class HirePointDto implements Serializable {
    private String id;

    @NotBlank(message = "hire point name required!")
    private String hirePointName;

    @NotBlank(message = "address1 required!")
    private String address1;

    private String address2;

    @NotBlank(message = "city is required!")
    private String city;

    @NotBlank(message = "country is required")
    private String country;

    @NotBlank(message = "postcode is required")
    private String postcode;

    @NotBlank(message = "mobile number is required!")
    @Min(value = 10, message = "mobile number incorrect!")
    private String mobileNumber;

    @NotBlank(message = "email required!")
    @Pattern(regexp = "^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$", message = "Email validation failed!")
    private String email;
}
