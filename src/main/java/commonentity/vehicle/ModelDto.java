package commonentity.vehicle;

import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
@Data
public class ModelDto implements Serializable {
    
    private String id;
    @NotBlank(message = "model name required!")
    private String modelName;
    @NotNull(message = "manufacturer Required!")
    @Valid
    private ManufacturerDto manufacturer;

}
