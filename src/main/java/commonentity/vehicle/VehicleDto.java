package commonentity.vehicle;


import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
public class VehicleDto implements Serializable {
    private String id;
    @NotBlank(message = "Plate Number Required!")
    private String plateNumber;
    @NotNull(message = "Model Required!")
    @Valid
    private ModelDto vehicle;
    private DiscountDto discount;
    @NotNull(message = "Hire Point Required!")
    @Valid
    private HirePointDto hirePoint;


}
